# This source code is provided for the purposes of scientific reproducibility
# under the following limited license from Element AI Inc. The code is an
# implementation of the N-BEATS model (Oreshkin et al., N-BEATS: Neural basis
# expansion analysis for interpretable time series forecasting,
# https://arxiv.org/abs/1905.10437). The copyright to the source code is
# licensed under the Creative Commons - Attribution-NonCommercial 4.0
# International license (CC BY-NC 4.0):
# https://creativecommons.org/licenses/by-nc/4.0/.  Any commercial use (whether
# for the benefit of third parties or internally in production) requires an
# explicit license. The subject-matter of the N-BEATS model and associated
# materials are the property of Element AI Inc. and may be subject to patent
# protection. No license to patents is granted hereunder (whether express or
# implied). Copyright © 2020 Element AI Inc. All rights reserved.

"""
Hierarchical Tourism Dataset
"""
import logging
import os
from dataclasses import dataclass

import numpy as np
import pandas as pd
import patoolib

from common.http_utils import download, url_file_name
from common.settings import DATASETS_PATH

DATASET_URL = 'https://raw.githubusercontent.com/carlomazzaferro/scikit-hts-examples/master/data/raw/visnights/visnights.csv'

DATASET_PATH = os.path.join(DATASETS_PATH, 'hTourism')
DATASET_FILE_PATH = os.path.join(DATASET_PATH, url_file_name(DATASET_URL))


@dataclass()
class hTourismMeta:
    seasonal_patterns = ['Quarterly']
    horizons = [8]
    frequency = [4]
    horizons_map = {
        'Quarterly': 8,
    }
    frequency_map = {
        'Quarterly': 4,
    }


@dataclass()
class hTourismDataset:
    ids: np.ndarray
    groups: np.ndarray
    horizons: np.ndarray
    values: np.ndarray
    hierarchy: dict()

    @staticmethod
    def load(training: bool = True) -> 'hTourismDataset':
        """
        Load Tourism dataset from cache.

        :param training: Load training part if training is True, test part otherwise.
        """
        ids = []
        groups = []
        horizons = []
        values = []


        visnights = pd.read_csv(DATASET_FILE_PATH)
        visnights["state_zone"] = visnights.apply(lambda x: f"{x['state']}{x['zone']}", axis=1)

        grouped_sections = visnights.groupby(["state", "state_zone"])
        edges_hierarchy = list(grouped_sections.groups.keys())

        second_level_nodes = ["NSW", "OTH", "QLD", "SAU", "VIC", "WAU"]
        root_node = "Total"

        root_edges = [(root_node, second_level_node) for second_level_node in second_level_nodes]
        
        root_edges += edges_hierarchy
        visnights_bottom_level = visnights.pivot(index="date", columns="state_zone", values="total_visitors_nights")

        def get_state_columns(df, state):
            return [col for col in df.columns if state in col]

        states = visnights["state"].unique().tolist()

        for state in states:
            state_cols = get_state_columns(visnights_bottom_level, state)
            visnights_bottom_level[state] = visnights_bottom_level[state_cols].sum(axis=1)

        visnights_bottom_level["Total"] = visnights_bottom_level[states].sum(axis=1)

        hierarchy = dict()

        horizons = hTourismMeta.horizons

        for edge in root_edges:
            parent, children = edge[0], edge[1]
            hierarchy.get(parent)
            if not hierarchy.get(parent):
                hierarchy[parent] = [children]
            else:
                hierarchy[parent] += [children]

        for group in hTourismMeta.seasonal_patterns:
            ids = visnights_bottom_level.columns.values
            groups.extend([group] * len(visnights_bottom_level.columns))
            visnights_bottom_level = visnights_bottom_level.reset_index(drop=True).T
    
        if(training):
            visnights_bottom_level = visnights_bottom_level.iloc[:,:-hTourismMeta.horizons_map[group]]
        else:
            visnights_bottom_level = visnights_bottom_level.iloc[:,-hTourismMeta.horizons_map[group]:]


        return hTourismDataset(ids=np.array(ids),
                              groups=np.array(groups),
                              horizons=np.array(horizons),
                              values=np.array(visnights_bottom_level),
                              hierarchy = hierarchy
                              )

    @staticmethod
    def download():
        """
        Download Tourism dataset.
        """
        if os.path.isdir(DATASET_PATH):
            logging.info(f'skip: {DATASET_PATH} directory already exists.')
            return
        download(DATASET_URL, DATASET_FILE_PATH)

    def to_hp_search_training_subset(self):
        return hTourismDataset(ids=self.ids,
                              groups=self.groups,
                              horizons=self.horizons,
                              values=np.array([v[:-self.horizons[i]] for i, v in enumerate(self.values)]),
                              hierarchy = self.hierarchy
                              )
